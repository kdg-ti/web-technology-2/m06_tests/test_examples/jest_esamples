import {expect,test,beforeAll,beforeEach} from "vitest";
let ra:number[];

beforeAll(() => {console.log("Doe de jest!")});

beforeEach(() => {  ra=[1,2,3];});

test("pop geeft array zonder laatste element met kortere lengte", () => {
  ra.pop();
  expect(ra).not.toContain(3);
  expect(ra).toHaveLength(2);
});
test("push voegt element toe", () => {
  ra.push(4);
  // vergelijk ELK VELD van object/ element van array
  // kan niet met toBe()
  expect(ra).toStrictEqual([1, 2, 3,4]);
});


