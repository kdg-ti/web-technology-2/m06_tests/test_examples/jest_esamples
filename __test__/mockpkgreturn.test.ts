import {vi, test, expect} from "vitest";
import {sumProd} from "../excel.js";

vi.mock("lodash",()=> ( {
  range:() => [8,9,10]
}))

vi.mock("lodash",()=>({
  range:() => vi.fn().mockReturnValue([8,9,10])(),
}));

test("mock range", () => {
  // proves that we are using the mock, the real implementation would return [1,2,3]
 // expect(range(1, 4)).toEqual([8,9,10]);
  //testing sumProd, but using mock instead of calling lodash.range
  expect(sumProd(8,11)).toBe(56);
  // this  works as well!!
  expect(sumProd(1,4)).toBe(56);
});
