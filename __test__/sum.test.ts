import {expect,test} from "vitest";
import {sum} from "../index.js";

test("2 getallen optellen geeft exacte som", () => {
    expect(sum(1, 2))
      .toBe(3);
  }
)

test("2 getallen optellen is niet gelijk aan / groter dan", () => {
    expect(sum(1, 2)).not.toBe(4);
    expect(sum(1, 2)).toBeGreaterThan(2);
})


test('There is no I in team', () => {
    expect('team').not.toMatch(/I/);
});
