import {vi,test,expect} from "vitest";
import {range} from 'lodash';
import {sumProd} from "../excel.js";


// fn(impl) is a shorthand for fn().mockImplemenation(impl)
vi.mock("lodash",()=>({
  range:(from:number,to:number) => vi.fn(() => {
    let result:number[] = [];
    for (let i:number = from; i < to; i++) {
      result.push(i);
    }
    return result;
  })(),
}))
test("mock range", () => {
  //use this test if providiing a real implementation
  expect(range(8, 11)).toEqual([8,9,10]);
  expect(sumProd(8,11)).toBe(56);
});
