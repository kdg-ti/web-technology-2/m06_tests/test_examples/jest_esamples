import lodash,{range} from 'lodash';
import {vi,test,expect} from "vitest";
import {sumProd} from "../excel.js";


 test("monitor lodash usage", () => {
    const spy = vi.spyOn(lodash, "range");
    const result =sumProd(8,11);
    expect(spy).toHaveBeenCalledWith(8, 11);
    expect(result).toEqual(56)
});
