import * as _ from "lodash";

export function sumProd(from:number,to:number){
    let result = 0;
    let series = _.range(from, to);
    for (let i=0;i<series.length;){
        result += series[i]*++i;
    }
    return result;
}
